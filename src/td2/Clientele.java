package td2;

import java.util.ArrayList;

public class Clientele {
	private ArrayList<Client> listeClient = new ArrayList<Client>();

	public Clientele(ArrayList<Client> listeClient) {
		super();
		this.listeClient = listeClient;
	}

	
	public Clientele() {
		super();
		this.listeClient= new ArrayList<Client>();
	}


	public ArrayList<Client> getListeClient() {
		return listeClient;
	}

	public void setListeClient(ArrayList<Client> listeClient) {
		this.listeClient = listeClient;
	}
	
public void add( Client c){
	this.listeClient.add(c);
}
public int indexCl(Client c) {
	
	
	
	return listeClient.indexOf(c);
	
	
}
public Client getClient(int index){
	return this.listeClient.get(index);
}

public void addCA(int num, float chiffre){
	Client c= this.listeClient.get(num);
	c.setcAClient(c.getcAClient() + chiffre);
}

public void affiche(){
	for (Client c : this.listeClient) {
		System.out.println(c);
		
	}
}
}
