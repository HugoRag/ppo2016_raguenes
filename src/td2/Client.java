package td2;

import java.util.Scanner;

import td1.Saisie;


public class Client {
	String nom;
	String prenom;
	float cAClient;
	
	public Client(String nom, String prenom, float cAClient) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.cAClient = cAClient;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public float getcAClient() {
		return cAClient;
	}
	public void setcAClient(float cAClient) {
		this.cAClient = cAClient;
	}
	@Override
	public  String toString() {
		return "Client [nom=" + nom + ", prenom=" + prenom + ", cAClient="
				+ cAClient + "]";
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(cAClient);
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		Client c= (Client) obj ;
		return c.getNom().equalsIgnoreCase(this.getNom()) && c.getPrenom().equalsIgnoreCase(this.getPrenom());
	}
	public void affiche(){
		System.out.println(this);
	}

	public static Client saisie(){
		return new Client(Saisie.lireChaine("Saisir prenom"), Saisie.lireChaine("Saisir nom"), Saisie.lireReel("saisir CA"));
	}
}
