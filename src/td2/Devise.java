package td2;

public class Devise {
	private String nomDevise;
	private float montantDevise;
	
	public Devise(String nomDevise, float montantDevise) {
		super();
		this.nomDevise = nomDevise;
		this.montantDevise = montantDevise;
	}
	public String getNomDevise() {
		return nomDevise;
	}
	public void setNomDevise(String nomDevise) {
		this.nomDevise = nomDevise;
	}
	public float getMontantDevise() {
		return montantDevise;
	}
	public void setMontantDevise(float montantDevise) {
		this.montantDevise = montantDevise;
	}
	
	
	 @Override
	public String toString() {
		return "Devise [nomDevise=" + nomDevise + ", montantDevise="
				+ montantDevise + "]";
	}
	 
	
	 @Override
	public boolean equals (Object o) {
		Devise d = (Devise) o;
	   	return this.getNomDevise().equalsIgnoreCase(d.getNomDevise());
	}
	 
	 
	 
	  
}
