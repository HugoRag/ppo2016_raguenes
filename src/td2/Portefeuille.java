package td2;

import java.util.ArrayList;

public class Portefeuille {
 

	private ArrayList<Devise> listeDevise = new ArrayList<Devise>();


	public Portefeuille(ArrayList<Devise> devise) {
		super();
		this.listeDevise = devise;
	}
	public ArrayList<Devise> getDevise() {
		return listeDevise;
	}
	public void setDevise(ArrayList<Devise> devise) {
		this.listeDevise = devise;
	}
    
   
     
	public void ajouterDevise( Devise d)
	{
		if(this.listeDevise.contains(d)) {
			Devise currentDevise = this.listeDevise.get(this.listeDevise.indexOf(d));
			currentDevise.setMontantDevise(currentDevise.getMontantDevise() + d.getMontantDevise());
		
		}
		else
		{
			this.listeDevise.add(d);
		}
		 
	}
	  public void retirerDevise(Devise d){
		  if(this.listeDevise.contains(d)) {
				Devise currentDevise = this.listeDevise.get(this.listeDevise.indexOf(d));
				if (currentDevise.getMontantDevise() >= d.getMontantDevise())
				{
					currentDevise.setMontantDevise(currentDevise.getMontantDevise() - d.getMontantDevise());
	
				}
				else {
					throw new IllegalArgumentException("Le montant est trop �lev�");
				}
		  }
		  else
		  {
				throw new IllegalArgumentException("Cette devise n'est pas dans le portefeuille");

		  }
	   }
}
