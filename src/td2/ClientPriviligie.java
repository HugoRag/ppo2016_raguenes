package td2;

public class ClientPriviligie extends Client {

	private Privilege privilege;

	public ClientPriviligie(String nom, String prenom, float cAClient) {
		super(nom, prenom, cAClient);
		
		if(Privilege.VIC.getClientCA() <= cAClient)
			{
				this.setPrivilege(Privilege.VIC);
				
			}
		else if (Privilege.CLIENT_EXCEPTIONNEL.getClientCA() <= cAClient)
			{
				this.setPrivilege(Privilege.CLIENT_EXCEPTIONNEL);
				
			}
		else if (Privilege.BON_CLIENT.getClientCA() <= cAClient)
			{
				this.setPrivilege(Privilege.BON_CLIENT);
			}
		
	}

	@Override
	public String toString() {
		return "ClientPriviligie [nom=" + this.getNom() + ", prenom=" +this.getPrenom() + ", cAClient="
				+ this.getcAClient() +", privilege=" + privilege + "]";
	}

	public Privilege getPrivilege() {
		return privilege;
	}

	public void setPrivilege(Privilege privilege) {
		this.privilege = privilege;
	}
}
