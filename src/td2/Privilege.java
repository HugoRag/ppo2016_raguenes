package td2;

public enum Privilege {
	BON_CLIENT(1000,0.05f),
	CLIENT_EXCEPTIONNEL(3000,0.15f),
	VIC(10000,0.3f);
	private int clientCA;
	private float remise=0;
	
	private Privilege(int clientCA, float remise) {
		this.clientCA = clientCA;
		this.remise = remise;
	}
	
	public int getClientCA() {
		return clientCA;
	}
	
	public void setClientCA(int clientCA) {
		this.clientCA = clientCA;
	}
	
	public float getRemise() {
		return remise;
	}
	
	public void setRemise(float remise) {
		this.remise = remise;
	}
		
}
