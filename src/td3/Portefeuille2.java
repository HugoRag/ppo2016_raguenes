package td3;

import java.io.Serializable;
import java.util.HashMap;

public class Portefeuille2 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7840713735823795777L;
	private int id;
	private String nomPf;
	HashMap<Devise2, Double> listeDevise = new HashMap<Devise2, Double>();

	public Portefeuille2(int id, String nomPf) {
		this.id = id;
		this.nomPf = nomPf;
		this.listeDevise = new HashMap<Devise2, Double>();
	}
	
	public Portefeuille2(String nomPf) {
		this(-1, nomPf);
	}

	@Override
	public String toString() {
		return "Portefeuille2 [nomPf=" + nomPf + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNomPf() {
		return nomPf;
	}

	public void setNomPf(String nomPf) {
		this.nomPf = nomPf;
	}

	public HashMap<Devise2, Double> getListeDevise() {
		return listeDevise;
	}

	public void ajouterDevise(Devise2 d, double montant) {
		if (this.listeDevise.containsKey(d)) {
			listeDevise.put(d, (listeDevise.get(d) + montant));
		} else {
			this.listeDevise.put(d, montant);
		}

	}

	public void retirerDevise(Devise2 d, double montant) {
		if (this.listeDevise.containsKey(d)) {
			Double currentDevise = this.listeDevise.get(d);
			if (montant <= listeDevise.get(d)) {
				listeDevise.put(d, (listeDevise.get(d) - montant));

			} else {
				throw new IllegalArgumentException("Le montant est trop �lev�");
			}
		} else {
			throw new IllegalArgumentException(
					"Cette devise n'est pas dans le portefeuille");

		}
	}
	
	
	public  void afficher()
	{
		for(Devise2 d : this.listeDevise.keySet()) {
			System.out.println(d +" " + String.valueOf(this.listeDevise.get(d)));
		}
		
	}
}
