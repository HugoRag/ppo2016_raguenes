package td3;

import java.io.Serializable;

public class Devise2 implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2319768803576167230L;
	private String nomDevise;
	
	public Devise2(String nomDevise) {
		super();
		this.nomDevise = nomDevise;
	
	}
	public String getNomDevise() {
		return nomDevise;
	}
	public void setNomDevise(String nomDevise) {
		this.nomDevise = nomDevise;
	}

	
	
	 @Override
	public String toString() {
		return "nom Devise= " + nomDevise + ", montant Devise";
	}
	 public int hashCode(){
		 return this.nomDevise.toUpperCase().hashCode();
	 }
	 
	
	 @Override
	public boolean equals (Object o) {
		Devise2 d = (Devise2) o;
	   	return this.getNomDevise().equalsIgnoreCase(d.getNomDevise());
	}
	 
	 
	 
	  
}
