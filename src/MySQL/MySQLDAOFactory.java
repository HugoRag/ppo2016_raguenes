package MySQL;
import DAO.DAOFactory;
import DAO.DeviseDAO;
import DAO.PortefeuilleDAO;
import DAO.*;
public class MySQLDAOFactory extends DAOFactory {
@Override
public PortefeuilleDAO getPortefeuilleDAO() {
return MySQLPortefeuilleDAO.getInstance();
}
@Override
public DeviseDAO getDeviseDAO() {
return MySQLDeviseDAO.getInstance();
}



}