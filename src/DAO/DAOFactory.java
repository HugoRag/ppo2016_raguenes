package DAO;

import MySQL.MySQLDAOFactory;

public abstract class DAOFactory {
public static DAOFactory getDAOFactory(int cible) {
DAOFactory daoF = null;
switch (cible) {
case 1:
daoF = new MySQLDAOFactory();
break;
//case Bin:
//daoF = new XMLDAOFactory();
//break;
}
return daoF;
}
public abstract PortefeuilleDAO getPortefeuilleDAO();
public abstract DeviseDAO getDeviseDAO();
}